#-------------------------------------------------
#
# Project created by QtCreator 2012-09-05T00:11:59
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = htmlit
CONFIG   += console c++11
CONFIG   -= app_bundle

TEMPLATE = app

## sorry everyone that wishes to use anything other than msvc :)
DEFINES += _CRT_SECURE_NO_WARNINGS
INCLUDEPATH += src

SOURCES += \
    src/RopFile.cpp \
    src/HTMLIt.cpp \
    src/File.cpp \
    src/WebkitExploit.cpp \
    src/firmwares/WebkitExploit150.cpp \
    src/firmwares/WebkitExploit1691.cpp \
    src/firmwares/WebkitExploit180.cpp

HEADERS += \
    src/Types.h \
    src/RopFile.h \
    src/HTMLIt.h \
    src/File.h \
    src/utils/Factory.h \
    src/WebkitExploitFactory.h \
    src/WebkitExploit.h \
    src/firmwares/WebkitExploit150.h \
    src/firmwares/WebkitExploit1691.h \
    src/firmwares/WebkitExploit180.h

RESOURCES += \
    src/firmwares/resources/FirmwareTemplates.qrc

OTHER_FILES += \
    src/firmwares/resources/html_template.html \
    src/firmwares/resources/150_exploit.js \
    src/firmwares/resources/1691_exploit.js \
    src/firmwares/resources/180_exploit.js
