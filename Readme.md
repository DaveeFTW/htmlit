**HTMLIt** - _webkit exploit kit for < 2.00 and roptool_

Currently, HTMLIt supports firmwares 1.50, 1.69 and 1.80/1.81. Its relatively simple to extend functionality to other firmwares. HTMLIt's main functionality is adding a ROPTool payload to to an exploitable webpage. The command line for doing this is:

htmlit -f [1.691/1.50/1.80] -o [output html] *source_file*

Happy coding!
Requires C++11 and Qt 5.2+