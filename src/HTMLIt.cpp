// HTMLIt.cpp : Defines the entry point for the console application.
//

// htmlit
#include "HTMLIt.h"
#include "File.h"
#include "RopFile.h"
#include "WebkitExploitFactory.h"

// qt
#include <QtCore/QCoreApplication>
#include <QtCore/QCommandLineParser>

// std
#include <algorithm>
#include <iostream>
#include <iterator>
#include <memory>

void HTMLIt::showInfo(void)
{
	// display some nice intro
	std::cout << "HTMLIt by Davee." << std::endl;
	std::cout << "URL: http://lolhax.org" << std::endl;
	std::cout << "Twitter: @DaveeFTW" << std::endl << std::endl;
}

std::shared_ptr<QCommandLineParser> HTMLIt::getCommandLine(const QCoreApplication& app)
{
    std::shared_ptr<QCommandLineParser> parser(new QCommandLineParser);

    parser->setApplicationDescription("HTMLIt - ROPTool injection for psvita webkit");
    parser->addHelpOption();
    parser->addVersionOption();

    QCommandLineOption firmwareOption(QStringList() << "f" << "firmware", "firmware version to build for.", "[firmware]");
    QCommandLineOption destinationOption(QStringList() << "o" << "destination", "output html file.", "[dest]");

    // set options
    parser->addPositionalArgument("source", "path to rop file.", "[source]");
    parser->addOption(firmwareOption);
    parser->addOption(destinationOption);


    // process this all
    parser->process(app);

    return parser;
}

int HTMLIt::start(const QCoreApplication& app)
{
    // read file
    RopFile rfile;

	// display program info
	showInfo();

	// create commandline
    std::shared_ptr<QCommandLineParser> cmd = getCommandLine(app);
	
	// process and handle commands
	try 
    {
		// check for no args
        if (cmd->isSet("help"))
		{
            // show help
            cmd->showHelp();
            return 1;
		}

		// check for firmware
        if (!cmd->isSet("firmware"))
		{
			std::cout << "Firmware version is required!";
			return 1;
		}

		// check for source ropfile
        if (cmd->positionalArguments().size() != 1)
		{
			std::cout << "RopFile is required." << std::endl;
			return 1;
		}

        // check for output
        if (!cmd->isSet("destination"))
        {
            std::cout << "Destination is required." << std::endl;
            return 1;
        }

        // create exploit
        WebkitExploit *exploit = WebkitExploitFactory::create(cmd->value("firmware"));

		// check if we found it
        if (!exploit)
		{
            std::cout << "Error: firmware '" << cmd->value("firmware").toStdString() << "' is not supported!" << std::endl;
			return 1;
		}
		
		// read our file
        int res = rfile.readFile(cmd->positionalArguments().at(0).toStdString());

		// check for error
		if (res < 0)
		{
			// error
            std::cout << "Error reading ROPFile: " << cmd->positionalArguments().at(0).toStdString() << std::endl;
			return 1;
		}

		// write file
        res = WriteFile(cmd->value("destination").toStdString(), exploit->data(rfile));
		
		if (res < 0)
		{
			// error
			std::cout << "Error writing exploit file!" << res << std::endl;
			return 1;		
		}
		
		// display complete
		std::cout << "Complete!" << std::endl;
    }

	// catch any exceptions
    catch(std::exception& e) 
	{
		// display the exception
        std::cerr << "Error: " << e.what() << std::endl;
		std::cerr << "Use --help for usage information" << std::endl;
        return 1;
    }
	
	// success
	return 0;
}

int main(int ac, char* av[])
{
    // pass args to core application
    QCoreApplication a(ac, av);
    QCoreApplication::setApplicationName("htmlit");
    QCoreApplication::setApplicationVersion("1.0");

    // create html it
	HTMLIt prog;
    return prog.start(a);
}
