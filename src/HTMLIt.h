#ifndef _HTMLIT_H_
#define _HTMLIT_H_

#include <QtCore/QCoreApplication>
#include <QtCore/QCommandLineParser>

#include <string>
#include <memory>

class HTMLIt
{
	public:
		// show program information
		static void showInfo(void);

		// start the application
        int start(const QCoreApplication& app);
		
		// get the command line
        std::shared_ptr<QCommandLineParser> getCommandLine(const QCoreApplication& app);
	
    private:
};

#endif //_HTMLIT_H_
