#ifndef _FILE_H_
#define _FILE_H_

// htmlit
#include "Types.h"

// qt
#include <QByteArray>

// std
#include <string>
#include <vector>

// read file
std::string ReadFileAscii(const std::string& file);
int ReadFile(const std::string& file, std::vector<u8>& buffer);

// write file
int WriteFile(const std::string& file, std::vector<u8>& data);
int WriteFile(const std::string& file, const QByteArray& data);

#endif //_FILE_H_
