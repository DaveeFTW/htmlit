#include "RopFile.h"

// htmlit
#include "File.h"

int RopFile::writeFile(const std::string& file)
{
	// set header
    m_header.magic = ROPFILE_MAGIC;
    m_header.version = ROPFILE_VERSION(1,0);
	m_header.daddr = m_data_address;
	m_header.dsize = m_data.size();
	m_header.centry = m_stack_address;
	m_header.csize = m_stack.size();
	
	// now allocate big buffer
	std::vector<u8> file_data(sizeof(m_header)+m_data.size()+m_stack.size(), 0);
	
	// copy data
	memcpy(file_data.data(), (u8 *)&m_header, sizeof(m_header));
	memcpy(file_data.data()+sizeof(m_header), m_data.data(), m_data.size());
	memcpy(file_data.data()+sizeof(m_header)+m_data.size(), m_stack.data(), m_stack.size());
	
	// write to file
	return WriteFile(file, file_data);
}

int RopFile::readFile(const std::string& file)
{
	std::vector<u8> file_data;
	
	// read the file
	int res = ReadFile(file, file_data);
	
	// check for error
	if (res < 0)
		return res;
		
	// parse header
    memcpy((u8 *)&m_header, file_data.data(), sizeof(m_header));

    // sort datas
    m_data = QByteArray(reinterpret_cast<char *>(file_data.data()+sizeof(m_header)),
                        m_header.dsize);

    // read stack
    m_stack = QByteArray(reinterpret_cast<char *>(file_data.data()+file_data.size()-m_header.csize),
                         m_header.csize);

	
	// set values
	m_data_address = m_header.daddr;
	m_stack_address = m_header.centry;
	
	// done
	return 0;
}

RopFile::RopFile(void)
{
    // construct
}

RopFile::~RopFile(void)
{
    // destruct
}
