#include "File.h"

// std
#include <fstream>

std::string ReadFileAscii(const std::string& file)
{
	// open the file
	std::ifstream fs(file);
	
	// check for error
	if (!fs)
	{
		// error, return empty array
		return std::string();
	}
	
	// read string with an iterator
	std::string str((std::istreambuf_iterator<char>(fs)), std::istreambuf_iterator<char>());

	// return the string
	return str;
}

int ReadFile(const std::string& file, std::vector<u8>& buffer)
{
	// open the file
	std::ifstream fs(file);
	
	// check for error
	if (!fs)
	{
		// error, return empty array
		return -1;
	}
	
	// get the size
	fs.seekg(0, std::ios::end);
	std::streamoff size = fs.tellg();
	fs.seekg(0, std::ios::beg);
	
	// create the script
	buffer.resize((u32)size);
	
	// read
	fs.read((char *)buffer.data(), size);
	fs.close();

	// return the file
	return 0;
}


int WriteFile(const std::string& file, const QByteArray& data)
{
    // open file for writing
    std::ofstream fs(file, std::ios::out | std::ios::trunc | std::ios::binary);

    // check for error
    if (!fs)
    {
        // error!
        return -1;
    }

    // write data
    fs.write(data.data(), data.size());
    fs.close();

    // done!
    return 0;
}

int WriteFile(const std::string& file, std::vector<u8>& data)
{
	// open file for writing
	std::ofstream fs(file, std::ios::out | std::ios::trunc | std::ios::binary);
	
	// check for error
	if (!fs)
	{
		// error!
		return -1;
	}
	
	// write data
	fs.write((char *)data.data(), data.size());
	fs.close();
	
	// done!
	return 0;
}
