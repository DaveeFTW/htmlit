#ifndef _ROP_FILE_H_
#define _ROP_FILE_H_

// htmlit
#include "Types.h"

// qt
#include <QByteArray>

// std
#include <string>
#include <vector>

class RopFile
{
	public:
		// constructor
		RopFile(void);
		
		// get data
        u32 getDataAddress(void) const { return m_data_address; }
        u32 getDataSize(void) const { return m_data.size(); }
        QByteArray getData(void) const { return m_data; }
		
		// get stack
        u32 getStackAddress(void) const { return m_stack_address; }
        u32 getStackSize(void) const { return m_stack.size(); }
        QByteArray getStack(void) const { return m_stack; }
		
		// set data
        void setData(u32 address, const QByteArray& data) { m_data_address = address; m_data = data; }
        void setStack(u32 address, const QByteArray& stack) { m_stack_address = address; m_stack = stack; }
		
		// write file
		int writeFile(const std::string& file);
		
		// read file
		int readFile(const std::string& file);
		
		// destructor
		~RopFile(void);
		
	private:
        #define ROPFILE_MAGIC           (0x7E504F52)
        #define ROPFILE_VERSION(x,y)    ((x << 0x10) | (y & 0xFFFF))

        typedef struct
        {
            u32 magic;              // 0x00
            u32 version;            // 0x04
            u32 reserved1;          // 0x08
            u32 reserved2;          // 0x0C
            u64 dsize;              // 0x10
            u64 daddr;              // 0x18
            u64 csize;              // 0x20
            u64 centry;             // 0x28
        } RopFileHeader;
		
		// header
		RopFileHeader m_header;

		// data elements
        QByteArray m_data;
        QByteArray m_stack;
		
		// addresses
		u32 m_data_address, m_stack_address;
};

#endif // _ROP_FILE_H_
